﻿using System;
using DmsReportWebApp.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DmsReportWebApp.Controllers
{
    public class EmployeeWorkingTimeQuery
    {
        public string CustomerID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StaffID { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
    
    public class EmployeeWorkingTimeReportController : Controller
    {
        private IOptions<AppConfigDto> _appConfig { get; }

        public EmployeeWorkingTimeReportController(IOptions<AppConfigDto> appConfig)
        {
            _appConfig = appConfig;
        }

        // GET
        public IActionResult Index([ModelBinder] EmployeeWorkingTimeQuery query)
        {
            PrepareReport(query);
            return View("ReportV2");
        }
        
        public IActionResult TimeReportStaffAndSup([ModelBinder] EmployeeWorkingTimeQuery query)
        {
            PrepareReport(query);
            return View("TimeReportStaffAndSup");
        }

        public IActionResult ReportV2([ModelBinder] EmployeeWorkingTimeQuery query)
        {
            PrepareReport(query);
            return View("ReportV2");
        }

        private void PrepareReport(EmployeeWorkingTimeQuery query)
        {
            var reportApiBaseUrl = _appConfig.Value.ReportApiBaseUrl.TrimEnd(new[] {'/'});
            var reportPath = string.IsNullOrEmpty(_appConfig.Value.EmployeeTimeReportApiPath) ? "/api/test/getTimeKeepingByDate" 
                : _appConfig.Value.EmployeeTimeReportApiPath;
            
            ViewData["ReportApiUrl"] = $"{reportApiBaseUrl}{reportPath}";
            ViewData["authToken"] = _appConfig.Value.AuthToken;
            ViewData["dateFrom"] = string.IsNullOrEmpty(query.StartDate)
                ? GetDefaultStartDate(DateTime.Now)
                : query.StartDate;
            ViewData["dateTo"] = string.IsNullOrEmpty(query.EndDate) ? GetDefaultEndDate(DateTime.Now) : query.EndDate;
            ViewData["customerId"] = string.IsNullOrEmpty(query.CustomerID) ? "1" : query.CustomerID;
            ViewData["staffId"] = string.IsNullOrEmpty(query.StaffID) ? "192" : query.StaffID;
            ViewData["page"] = query.Page > 0 ? query.Page : 1;
            ViewData["pageSize"] = query.PageSize > 0 ? query.PageSize : 20;
            ViewData["appVersion"] = _appConfig.Value.AppVersion;
        }

        string GetDefaultStartDate(DateTime d) => $"{d.Year}-{d.Month}-01";
        string GetDefaultEndDate(DateTime d) => DateTime.Today.ToString("yyyy-MM-dd"); //$"{d.Year}-{d.Month}-{DateTime.DaysInMonth(d.Year, d.Month)}";

        [Route("AdaptiveRendering")]
        public IActionResult TestGridAdaptiveRendering()
        {
            return View("AdaptiveRendering");
        }
        
        [Route("ReportFrame")]
        public IActionResult ReportFrame()
        {
            return View("ReportFrame");
        }
    }
}
