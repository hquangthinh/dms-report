﻿using System.Collections.Generic;
using DmsReportWebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace DmsReportWebApp.Controllers
{
    public class HelpIndexQuery
    {
        public string P1 { get; set; }
        public string P2 { get; set; }
    }
    
    public class HelpController : Controller
    {
        public IActionResult Index([ModelBinder] HelpIndexQuery query)
        {
            var model = GetHelpContent(query);

            return View("Index", model);
        }

        IList<HelpContentItem> GetHelpContent(HelpIndexQuery query)
        {
            if (query == null || string.IsNullOrEmpty(query.P1) || string.IsNullOrEmpty(query.P2) ||
                "test".Equals(query.P1))
            {
                return GenerateDefaultHelpContent();
            }

            return QueryHelpConent(query);
        }

        private IList<HelpContentItem> GenerateDefaultHelpContent()
        {
            return new List<HelpContentItem>
            {
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/underwater.jpg",
                    ImageCaption = "underwater"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/rocks.jpg",
                    ImageCaption = "rocks"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/mountainskies.jpg",
                    ImageCaption = "mountainskies"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/underwater.jpg",
                    ImageCaption = "underwater"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/rocks.jpg",
                    ImageCaption = "rocks"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/mountainskies.jpg",
                    ImageCaption = "mountainskies"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/underwater.jpg",
                    ImageCaption = "underwater"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/rocks.jpg",
                    ImageCaption = "rocks"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/wedding.jpg",
                    ImageCaption = "wedding"
                },
                new HelpContentItem
                {
                    ImageUrl = "https://www.w3schools.com/w3images/mountainskies.jpg",
                    ImageCaption = "mountainskies"
                },
            };
        }

        private IList<HelpContentItem> QueryHelpConent(HelpIndexQuery query)
        {
            throw new System.NotImplementedException("Query content from db");
        }
    }
}