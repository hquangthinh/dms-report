namespace DmsReportWebApp.Dto
{
    public class AppConfigDto
    {
        public string ReportApiBaseUrl { get; set; }
        public string EmployeeTimeReportApiPath { get; set; }
        public string AuthToken { get; set; }
        public string AppVersion { get; set; }
    }
}