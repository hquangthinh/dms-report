﻿var ssMoneyLevel = "0";
var smMoneyLevel = "0";
var headerWorkingDay = "0";
var avgWorkingRate = "0";
var sumWorkingMoney = "0";

function showDialog(content) {
  if(!content || content === "")
    return;
  $("<div/>").appendTo($("body")).kendoDialog({
    width: "200px",
    title: false,
    closable: false,
    modal: true,
    content: content,
    actions: [
      { text: 'Đóng' },
    ]
  }).data("kendoDialog").open();
}

(function($, kendo) {
  "use strict";

  // var pageSize = 20;
  // var dtNow = new Date();
  // var dtEnd = new Date();
  // dtEnd.setDate(dtEnd.getDate() + 7);
  // var startDate = dtNow.getFullYear() + "-" + (dtNow.getMonth() + 1) + "-" + dtNow.getDate();
  // var endDate = dtEnd.getFullYear() + "-" + (dtEnd.getMonth() + 1) + "-" + dtEnd.getDate();

  function createDataSource(reportApiUrl, startDate, endDate, pageSize) {
    return new kendo.data.DataSource({
      transport: {
        read: {
          url: reportApiUrl,
          type: "POST",
          headers: {
            "Authorization": "Bearer " + $("#hidAuthToken").val()
          },
          data: function() {
            return {
              StartDate: startDate,
              EndDate: endDate,
              CustomerID: $("#hidCustomerId").val(),
              StaffID: $("#hidStaffId").val(),
              Page: $("#hidPage").val(),
              PageSize: $("#hidPageSize").val()
            };
          },
          cache: false,
        },
      },
      requestEnd: function (e) {
        var response = e.response;
        if(!response)
          return [];
        var avgRate = 0;
        var sumWorkMoney = 0;
        var formatedData = response.map(function(item) {
          item.WorkingMoneyNum = kendo.parseFloat(item.WorkingMoney);
          sumWorkMoney += item.WorkingMoneyNum;
          avgRate += kendo.parseFloat(item.WorkingRate);
          item.WorkingMoney = formatAsNumber(item.WorkingMoney);
          //item.StaffName = item.StaffName + " (GSBH GSBH GSBH)"; // for testing
          if(item.TimeKeepingDetails && Array.isArray(item.TimeKeepingDetails) && item.TimeKeepingDetails.length > 0) {
            item.TimeKeepingDetails = item.TimeKeepingDetails.map(function(detailItem) {
              detailItem.NumVisit = setDefaultValue(detailItem.NumVisit);
              detailItem.DayOFF = setDefaultValue(detailItem.DayOFF);
              detailItem.OnTime = setDefaultValue(detailItem.OnTime);
              detailItem.NumStoreIMG = setDefaultValue(detailItem.NumStoreIMG);
              detailItem.NumOrder = setDefaultValue(detailItem.NumOrder);
              detailItem.FirstMeetIMG = setDefaultValue(detailItem.FirstMeetIMG);
              detailItem.LastMeetIMG = setDefaultValue(detailItem.LastMeetIMG);
              return detailItem;
            });
          }
          return item;
        });

        if(formatedData.length > 0) {
          ssMoneyLevel = formatAsNumber(formatedData[0].SSMoneyLevel);
          smMoneyLevel = formatAsNumber(formatedData[0].SMMoneyLevel);
          headerWorkingDay = formatedData[0].WorkingDay;
          sumWorkingMoney = formatAsNumber(sumWorkMoney);
          avgWorkingRate = formatAsNumber(avgRate/formatedData.length);
        }

        return formatedData;
      },
      pageSize: $("#hidPageSize").val() || pageSize
    });
  }

  $(document).ready(function () {
    const pageSize = $("#hidPageSize").val();
    const startDate = $("#hidDateFrom").val();
    const endDate = $("#hidDateTo").val();
    const apiDataSource = createDataSource(getReportApiUrl(), startDate, endDate, pageSize);

    $("#grid").kendoGrid({
      dataSource: apiDataSource,
      height: window.screen.height - 200,
      mobile: true,
      filterable: true,
      groupable: false,
      sortable: true,
      scrollable: {
        endless: true
      },
      pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
      },
      detailInit: detailInit,
      dataBound: function () {
        this.expandRow(this.tbody.find("tr.k-master-row").first());
        $("#ssMoneyLevel").text(ssMoneyLevel);
        $("#smMoneyLevel").text(smMoneyLevel);
        $("#headerWorkingDay").text(headerWorkingDay);
      },
      columns: [{
        field: "RankNo",
        title: "STT",
        filterable: false,
        headerTemplate: "<span class='app-grid-header'>STT</span>",
        headerAttributes: {
          style: "text-align: left;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: left;",
          class: "app-responsive-font",
        },
        width: "50px"
      },
        {
          field: "StaffName",
          title: "NVBH",
          filterable: { multi: true, search: true},
          template: "<span class='text-primary'><strong>#: StaffName # </strong></span>",
          headerTemplate: "<span class='app-grid-header'>NVBH</span>",
          footerTemplate: "<span class='text-primary'><strong>Tổng Cộng</strong></span>",
          headerAttributes: {
            style: "text-align: left;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: left;",
            class: "app-responsive-font",
          },
          width: "200px"
        },
        {
          field: "CorrectDay",
          title: "NTT",
          filterable: false,
          template: "<strong>#: CorrectDay # </strong>",
          headerTemplate: "<span class='app-grid-header'>NTT</span>",
          headerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "RealDay",
          title: "NC",
          filterable: false,
          template: "<strong>#: RealDay # </strong>",
          headerTemplate: "<span class='app-grid-header'>NC</span>",
          headerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "WorkingRate",
          title: "TT %",
          filterable: false,
          template: "<strong>#: WorkingRate # </strong>",
          headerTemplate: "<span class='app-grid-header'>TT %</span>",
          footerTemplate: "<span class='text-primary'><strong>#:avgWorkingRate#</strong></span>",
          headerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          footerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "WorkingRate",
          title: "NC %",
          filterable: false,
          template: "<strong>#: WorkingRate # </strong>",
          headerTemplate: "<span class='app-grid-header'>NC %</span>",
          footerTemplate: "<span class='text-primary'><strong>#:avgWorkingRate#</strong></span>",
          headerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          footerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "WorkingMoney",
          title: "Tiền CL",
          filterable: false,
          template: "<strong>#: WorkingMoney # </strong>",
          headerTemplate: "<span class='app-grid-header'>Tiền CL</span>",
          format: "{0:c}",
          footerTemplate: "<span class='text-primary'><strong>#:sumWorkingMoney#</strong></span>",
          headerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          attributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          footerAttributes: {
            style: "text-align: right;",
            class: "app-responsive-font",
          },
          width: "200px"
        },
        {
          field: "WorkingMoney",
          title: "&nbsp;",
          template: "&nbsp;",
          filterable: false,
        }]
    });

    $("#dpFrom").kendoDatePicker({ format: "yyyy-MM-dd" });
    $("#dpTo").kendoDatePicker({ format: "yyyy-MM-dd" });
    $("#btnViewReport").click(function () {
      const pageSize = $("#hidPageSize").val();
      const startDate = $("#dpFrom").val();
      const endDate = $("#dpTo").val();
      const grid = $("#grid").data("kendoGrid");
      const ds = createDataSource(getReportApiUrl(), startDate, endDate, pageSize);
      grid.setDataSource(ds);
      grid.refresh();
    });
  });

  function detailInit(e) {
    if(!e.data || !e.data.TimeKeepingDetails || e.data.TimeKeepingDetails.length === 0) {
      return;
    }

    var itemDetailDs = {
      data: e.data.TimeKeepingDetails
    };
    $("<div/>").attr("class", "app-grid app-grid-detail").appendTo(e.detailCell).kendoGrid({
      dataSource: itemDetailDs,
      scrollable: true,
      sortable: false,
      pageable: false,
      columns: [
        {
          field: "TimeKeepingDay",
          title: "Ngày",
          template: "<span onclick='showDialog(\"#:Reason#\")'><strong>#:TimeKeepingDay#</strong></span>",
          headerTemplate: "<span class='app-grid-header'>Ngày</span>",
          attributes: {
            class: "app-responsive-font",
            style: "color: blue; text-decoration: underline; text-align: center;"
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "Ngày TT",
          template: "<span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> TT</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "NC",
          template: "<span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> Công</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "DB",
          template: "<span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Đồng <br/> Bộ</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "MCP",
          template: "40",
          headerTemplate: "<span class='app-grid-header'>MCP</span>",
          attributes: {
            class: "app-responsive-font",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Chở <br/> Hàng</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Hình CH <br/> Đầu Tiên</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<img class='app-responsive-icon' src='/images/#:NumVisit#.png' /> &nbsp; <span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Toàn <br/> Cảnh</span>",
          attributes: {
            class: "app-responsive-font",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<img class='app-responsive-icon' src='/images/#:NumVisit#.png' /> &nbsp; <span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Trưng <br/> Bày</span>",
          attributes: {
            class: "app-responsive-font",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<img class='app-responsive-icon' src='/images/#:NumVisit#.png' /> &nbsp; <span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Đơn <br/> Hàng</span>",
          attributes: {
            class: "app-responsive-font",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        },
        {
          field: "TimeKeepingDay",
          title: "HCH",
          template: "<img class='app-responsive-icon' src='/images/#:NumVisit#.png' /> &nbsp; <span>Đạt</span>",
          headerTemplate: "<span class='app-grid-header'>Doanh <br/> Số</span>",
          attributes: {
            class: "app-responsive-font",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          }
        }
      ]
    });
  }
})(window.jQuery, window.kendo);
