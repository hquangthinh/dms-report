var ssMoneyLevel = "0";
var smMoneyLevel = "0";
var headerWorkingDay = "0";
var headerCustomerWorkingDay = "0";
var footerAvgComplianceRate = "0";
var footerAvgTimesheetRate = "0";
var footerSumComplianceMoney = "0";
var isStaffRole = false;
var isMobile = false;

function showDialog(content) {
  if(!content || content === "")
    return;
  $("<div/>").appendTo($("body")).kendoDialog({
    width: "200px",
    title: false,
    closable: false,
    modal: true,
    content: content,
    actions: [
      { text: 'Đóng' },
    ]
  }).data("kendoDialog").open();
}

(function($, kendo) {
  "use strict";

  function createDataSource(reportApiUrl, startDate, endDate, pageSize) {
    return new kendo.data.DataSource({
      transport: {
        read: {
          url: reportApiUrl,
          type: "POST",
          headers: {
            "Authorization": "Bearer " + $("#hidAuthToken").val()
          },
          data: function() {
            return {
              StartDate: startDate,
              EndDate: endDate,
              CustomerID: $("#hidCustomerId").val(),
              StaffID: $("#hidStaffId").val(),
              Page: $("#hidPage").val(),
              PageSize: $("#hidPageSize").val() || pageSize
            };
          },
          cache: false,
        },
      },
      requestStart: function(e) {
        $('<div id="loader" class="spinner spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div>').appendTo("#grid .k-grid-content");
      },
      change: function(e) {
        $(".spinner").remove();
      },
      requestEnd: function (e) {
        var response = e.response;
        if(!response)
          return [];
        var avgComplianceRate = 0;
        var avgTimesheetRate = 0;
        var sumComplianceMoneyNum = 0;
        var formatedData = response.map(function(item) {
          avgComplianceRate += kendo.parseFloat(item.ComplianceRate);
          avgTimesheetRate += kendo.parseFloat(item.TimeSheetRate);
          sumComplianceMoneyNum += kendo.parseFloat(item.ComplianceMoney);
          if(item.TimeKeepingDetails && Array.isArray(item.TimeKeepingDetails) && item.TimeKeepingDetails.length > 0) {
            item.TimeKeepingDetails = item.TimeKeepingDetails.map(function(detailItem) {
              var onComplianceObj = parseMultiValueField(detailItem.OnCompliance);
              detailItem.OnComplianceBgColor = onComplianceObj[0];
              detailItem.OnComplianceValue = onComplianceObj[1];

              var onTimeSheetObj = parseMultiValueField(detailItem.OnTimeSheet);
              detailItem.OnTimeSheetBgColor = onTimeSheetObj[0];
              detailItem.OnTimeSheetValue = onTimeSheetObj[1];

              var isSyncObj = parseMultiValueField(detailItem.isSync);
              detailItem.isSyncBgColor = isSyncObj[0];
              detailItem.isSyncValue = isSyncObj[1];

              // Is Staff
              if(detailItem.StaffType === "1") {
                // WW NV
                detailItem.WWNVBgColor = "#ffffff";
                detailItem.WWNVValue = "";

                // MCP -> "NumOfVisit": "#ffffff||27"
                var numOfVisitObj = parseMultiValueField(detailItem.NumOfVisit);
                detailItem.MCPBgColor = numOfVisitObj[0];
                detailItem.MCPValue = numOfVisitObj[1];

                // HCH -> "BikeGoods": "#ffffff||K.Đ"
                var bikeGoodsObj = parseMultiValueField(detailItem.BikeGoods);
                detailItem.BikeGoodsBgColor = bikeGoodsObj[0];
                detailItem.BikeGoodsValue = bikeGoodsObj[1];
              } else {
                // Sales sup
                // WW NV
                var numOfVisitObj = parseMultiValueField(detailItem.NumOfVisit);
                detailItem.WWNVBgColor = numOfVisitObj[0];
                var nameList = numOfVisitObj[1] || "";
                detailItem.WWNVValue = isMobile ? nameList : nameList.replace(/,/g, "<br/>");

                // MCP
                var bikeGoodsObj = parseMultiValueField(detailItem.BikeGoods);
                detailItem.MCPBgColor = bikeGoodsObj[0];
                detailItem.MCPValue = bikeGoodsObj[1] || "";

                // HCH
                detailItem.BikeGoodsBgColor = "#ffffff";
                detailItem.BikeGoodsValue = "";
              }

              var firstStoreIMGObj = parseMultiValueField(detailItem.FirstStoreIMG);
              detailItem.FirstStoreIMGBgColor = firstStoreIMGObj[0];
              detailItem.FirstStoreIMGValue = firstStoreIMGObj[1];

              var firstPMStoreIMGObj = parseMultiValueField(detailItem.FirstPMStoreIMG);
              detailItem.FirstPMStoreIMGBgColor = firstPMStoreIMGObj[0];
              detailItem.FirstPMStoreIMGValue = firstPMStoreIMGObj[1];

              var fullStoreIMGObj = parseMultiValueField(detailItem.FullStoreIMG);
              detailItem.FullStoreIMGBgColor = fullStoreIMGObj[0];
              detailItem.FullStoreIMGFlagImg = fullStoreIMGObj[1]  || "0.png";
              detailItem.FullStoreIMGValue = fullStoreIMGObj[2] || fullStoreIMGObj[1] || "";

              var fullPOSMIMGObj = parseMultiValueField(detailItem.FullPOSMIMG);
              detailItem.FullPOSMIMGGBgColor = fullPOSMIMGObj[0];
              detailItem.FullPOSMIMGFlagImg = fullPOSMIMGObj[1]  || "0.png";
              detailItem.FullPOSMIMGValue = fullPOSMIMGObj[2] || fullStoreIMGObj[1] || "";

              var lastMeetingIMGObj = parseMultiValueField(detailItem.LastMeetingIMG);
              detailItem.LastMeetingIMGBgColor = lastMeetingIMGObj[0];
              detailItem.LastMeetingIMGValue = lastMeetingIMGObj[1];

              var numOfOrderObj = parseMultiValueField(detailItem.NumOfOrder);
              detailItem.NumOfOrderBgColor = numOfOrderObj[0];
              detailItem.NumOfOrderFlagImg = numOfOrderObj[1];
              detailItem.NumOfOrderValue = numOfOrderObj[2] || "0";

              var revenueObj = parseMultiValueField(detailItem.Revenue);
              detailItem.RevenueBgColor = revenueObj[0];
              detailItem.RevenueFlagImg = revenueObj[1];
              detailItem.RevenueValue = formatAsNumber(revenueObj[2]);

              return detailItem;
            });
          }
          return item;
        });

        if(formatedData.length > 0) {
          isStaffRole = formatedData[0].StaffType === "1";
          ssMoneyLevel = formatAsNumber(formatedData[0].SSMoneyLevel);
          smMoneyLevel = formatAsNumber(formatedData[0].SMMoneyLevel);
          headerWorkingDay = formatedData[0].WorkingDay;
          headerCustomerWorkingDay = formatedData[0].OnTimeSheetDay;
          footerSumComplianceMoney = formatAsMoneyNumber(sumComplianceMoneyNum);
          footerAvgComplianceRate = formatAsNumber(avgComplianceRate/formatedData.length);
          footerAvgTimesheetRate = formatAsNumber(avgTimesheetRate/formatedData.length);
        }

        return formatedData;
      },
      pageSize: $("#hidPageSize").val() || pageSize
    });
  }

  $(document).ready(function () {

    isMobile = Boolean(kendo.support.mobileOS);

    const gridHeight = isMobile ? window.screen.height - 100 : window.screen.height - 200;

    $("#grid").kendoGrid({
      height: gridHeight,
      mobile: true,
      filterable: true,
      groupable: false,
      sortable: true,
      scrollable: {
        endless: true
      },
      pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5,
        messages: {
          display: "Hiển thị {0}-{1} từ {2} bản ghi",
          empty: "Không có dữ liệu",
          itemsPerPage: "Bản ghi / trang"
        }
      },
      noRecords: true,
      messages: {
        noRecords: "Báo cáo chưa có dữ liệu"
      },
      detailInit: detailInit,
      dataBound: function () {
        this.expandRow(this.tbody.find("tr.k-master-row").first());
        $("#ssMoneyLevel").text(ssMoneyLevel);
        $("#smMoneyLevel").text(smMoneyLevel);
        $("#headerWorkingDay").text(headerWorkingDay);
        var staffNameHeaderText = isStaffRole ? "Nhân viên" : "Nhân viên";
        $("#headerTextStaffName").text(staffNameHeaderText);
      },
      columns: [{
        field: "RankNo",
        title: "STT",
        filterable: false,
        headerTemplate: "<span class='app-grid-header'>STT</span>",
        headerAttributes: {
          style: "text-align: center;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: center;",
          class: "app-responsive-font",
        },
        width: "50px"
      },
      {
        field: "StaffName",
        title: "Nhân viên bán hàng",
        filterable: { multi: true, search: true},
        template: "<span class='text-primary'><strong>#: StaffName # </strong></span>",
        headerTemplate: "<span id='headerTextStaffName' class='app-grid-header'>Nhân viên bán hàng</span>",
        footerTemplate: "<span class='text-primary'><strong>Tổng Cộng</strong></span>",
        headerAttributes: {
          style: "text-align: left;color: blue;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: left;color: blue;",
          class: "app-responsive-font",
        },
        width: "200px"
      },
      {
        field: "OnComplianceDay",
        title: "Ngày tuân thủ",
        filterable: false,
        template: "<strong>#: OnComplianceDay # </strong>",
        headerTemplate: "<span class='app-grid-header'>Ngày tuân thủ</span>",
        headerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        width: "100px"
      },
      {
        field: "OnTimeSheetDay",
        title: "Ngày công",
        filterable: false,
        template: "<strong>#: OnTimeSheetDay # </strong>",
        headerTemplate: "<span class='app-grid-header'>Ngày công</span>",
        headerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        width: "100px"
      },
      {
        field: "ComplianceRate",
        title: "% Đạt tuân thủ",
        filterable: false,
        template: "<strong>#: ComplianceRate # </strong>",
        headerTemplate: "<span class='app-grid-header'>% Đạt tuân thủ</span>",
        footerTemplate: "<span class='text-primary'><strong>#:footerAvgComplianceRate#</strong></span>",
        headerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        footerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        width: "100px"
      },
      {
        field: "TimeSheetRate",
        title: "Ngày công",
        filterable: false,
        template: "<strong>#: TimeSheetRate # </strong>",
        headerTemplate: "<span class='app-grid-header'>% Ngày công</span>",
        footerTemplate: "<span class='text-primary'><strong>#:footerAvgTimesheetRate#</strong></span>",
        headerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        footerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        width: "100px"
      },
      {
        field: "ComplianceMoney",
        title: "Tiền tuân thủ",
        filterable: false,
        template: "<strong>#: ComplianceMoney # </strong>",
        headerTemplate: "<span class='app-grid-header'>Tiền tuân thủ</span>",
        format: "{0:c}",
        footerTemplate: "<span class='text-primary'><strong>#:footerSumComplianceMoney#</strong></span>",
        headerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        attributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        footerAttributes: {
          style: "text-align: right;",
          class: "app-responsive-font",
        },
        width: "200px"
      },
      {
        field: "ComplianceMoney",
        title: "&nbsp;",
        template: "&nbsp;",
        filterable: false,
        width: "1px",
        hidden: true
      }]
    });

    const dpStartDate = $("#dpFrom").kendoDatePicker({ format: "yyyy-MM-dd", change: startDateChange }).data("kendoDatePicker");
    const dpEndDate = $("#dpTo").kendoDatePicker({ format: "yyyy-MM-dd" }).data("kendoDatePicker");

    const defaultStartDate = dpStartDate.value();
    dpEndDate.min(defaultStartDate);
    const defaultLastDayOfMonth = kendo.date.lastDayOfMonth(defaultStartDate);
    dpEndDate.max(new Date(defaultStartDate.getFullYear(), defaultStartDate.getMonth(), defaultLastDayOfMonth.getDate()));

    function startDateChange() {
      const startDate = dpStartDate.value();
      const lastDayOfMonth = kendo.date.lastDayOfMonth(startDate);
      dpEndDate.min(startDate);
      dpEndDate.max(new Date(startDate.getFullYear(), startDate.getMonth(), lastDayOfMonth.getDate()));
      dpEndDate.value(startDate);
    }

    $("#btnViewReport").click(function () {
      const pageSize = $("#hidPageSize").val();
      const startDate = $("#dpFrom").val();
      const endDate = $("#dpTo").val();
      const grid = $("#grid").data("kendoGrid");
      const ds = createDataSource(getReportApiUrl(), startDate, endDate, pageSize);
      grid.setDataSource(ds);
      grid.refresh();
    });

    $("#btnViewReport").click();
  });

  function detailInit(e) {
    if(!e.data || !e.data.TimeKeepingDetails || e.data.TimeKeepingDetails.length === 0) {
      return;
    }

    var itemDetailDs = {
      data: e.data.TimeKeepingDetails
    };

    var detailGridColumns = [];

    var isStaff = e.data.StaffType === "1";

    if(isStaff) {
      detailGridColumns = [
        {
          field: "TimeKeepingDay",
          title: "Ngày",
          template: "<span onclick='showDialog(\"#:Reason#\")'><strong>#:TimeKeepingDay#</strong></span>",
          headerTemplate: "<span class='app-grid-header'>Ngày</span>",
          attributes: {
            class: "app-responsive-font",
            style: "color: blue; text-decoration: underline; text-align: center;"
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "50px" //100
        },
        {
          field: "OnCompliance",
          title: "Ngày TT",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.OnComplianceBgColor + "'>" + dataItem.OnComplianceValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> TT</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:OnComplianceBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px" //100
        },
        {
          field: "OnTimeSheet",
          title: "NC",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.OnTimeSheetBgColor + "'>" + dataItem.OnTimeSheetValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> Công</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:OnTimeSheetBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px" //100
        },
        {
          field: "isSync",
          title: "DB",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.isSyncBgColor + "'>" + dataItem.isSyncValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Đồng <br/> Bộ</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:isSyncBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px" //100
        },
        {
          field: "NumOfVisit",
          title: "MCP",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.MCPBgColor + "'>" + dataItem.MCPValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>MCP</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center;background-color: #:MCPBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "120px" //100
        },
        {
          field: "BikeGoods",
          title: "HCH",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.BikeGoodsBgColor + "'>" + dataItem.BikeGoodsValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Chở Hàng</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:BikeGoodsBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px" //100
        },
        {
          field: "FirstStoreIMG",
          title: "HCH",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.FirstStoreIMGBgColor + "'>" + dataItem.FirstStoreIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình CH <br/> Đầu Tiên <br/> lúc 9h30</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:FirstStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px" //100
        },
        {
          field: "FirstPMStoreIMG",
          title: "HCH",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.FirstPMStoreIMGBgColor + "'>" + dataItem.FirstPMStoreIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình CH <br/> Đầu Tiên <br/> lúc 14h30</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:FirstPMStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px" //100
        },
        {
          field: "FullStoreIMG",
          title: "HTC",
          template: "<img style='background-color: #:FullStoreIMGBgColor#' class='app-responsive-icon' src='/images/#:FullStoreIMGFlagImg#' /> &nbsp; <span>#:FullStoreIMGValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Toàn Cảnh</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center;background-color: #:FullStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px"
        },
        {
          field: "FullPOSMIMG",
          title: "HTB",
          template: "<img style='background-color: #:FullPOSMIMGGBgColor#' class='app-responsive-icon' src='/images/#:FullPOSMIMGFlagImg#' /> &nbsp; <span>#:FullPOSMIMGValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Hình <br/> Trưng Bày</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center;background-color: #:FullPOSMIMGGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "90px"
        },
        {
          field: "LastMeetingIMG",
          title: "HHCN",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.LastMeetingIMGBgColor + "'>" + dataItem.LastMeetingIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình Họp <br/> Cuối Ngày</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center;background-color: #:LastMeetingIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "90px"
        },
        {
          field: "NumOfOrder",
          title: "DH",
          template: "<img style='background-color: #:NumOfOrderBgColor#' class='app-responsive-icon' src='/images/#:NumOfOrderFlagImg#' /> &nbsp; <span>#:NumOfOrderValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Đơn <br/> Hàng</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center;background-color: #:NumOfOrderBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "Revenue",
          title: "DS",
          template: "<img style='background-color: #:RevenueBgColor#' class='app-responsive-icon' src='/images/#:RevenueFlagImg#' /> &nbsp; <span>#:RevenueValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Doanh <br/> Số</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center;background-color: #:RevenueBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "150px"
        }
      ]
    } else {
      // GSBH
      detailGridColumns = [
        {
          field: "TimeKeepingDay",
          title: "Ngày",
          template: "<span onclick='showDialog(\"#:Reason#\")'><strong>#:TimeKeepingDay#</strong></span>",
          headerTemplate: "<span class='app-grid-header'>Ngày</span>",
          attributes: {
            class: "app-responsive-font",
            style: "color: blue; text-decoration: underline; text-align: center;"
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "50px"
        },
        {
          field: "OnCompliance",
          title: "Ngày TT",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.OnComplianceBgColor + "'>" + dataItem.OnComplianceValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> TT</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:OnComplianceBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px"
        },
        {
          field: "OnTimeSheet",
          title: "NC",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.OnTimeSheetBgColor + "'>" + dataItem.OnTimeSheetValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Ngày <br/> Công</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:OnTimeSheetBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px"
        },
        {
          field: "isSync",
          title: "DB",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.isSyncBgColor + "'>" + dataItem.isSyncValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Đồng <br/> Bộ</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:isSyncBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "60px"
        },
        {
          field: "WWNV",
          title: "WWNV",
          template: function (dataItem) {
            return "<span onclick='showDialog(\"" + dataItem.WWNVValue + "\")' style='background-color: " + dataItem.WWNVBgColor + "'>" + dataItem.WWNVValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>W*w <br/> cùng <br/> NV</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:WWNVBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "120px"
        },
        {
          field: "NumOfVisit",
          title: "MCP",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.MCPBgColor + "'>" + dataItem.MCPValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>MCP</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:MCPBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px"
        },
        {
          field: "FirstStoreIMG",
          title: "HCH",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.FirstStoreIMGBgColor + "'>" + dataItem.FirstStoreIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình họp <br/> đầu ngày</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:FirstStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px"
        },
        {
          field: "FirstPMStoreIMG",
          title: "HCH",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.FirstPMStoreIMGBgColor + "'>" + dataItem.FirstPMStoreIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình <br/> chở hàng</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:FirstPMStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px"
        },
        {
          field: "FullStoreIMG",
          title: "HTC",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.FullStoreIMGBgColor + "'>" + dataItem.FullStoreIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình CH <br/> đầu tiên <br/> lúc 9h30</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:FullStoreIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "80px"
        },
        {
          field: "FullPOSMIMG",
          title: "HTB",
          template: "<img style='background-color: #:FullPOSMIMGGBgColor#' class='app-responsive-icon' src='/images/#:FullPOSMIMGFlagImg#' /> &nbsp; <span>#:FullPOSMIMGValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Hình điểm <br/> bán có NV</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:FullPOSMIMGGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "90px"
        },
        {
          field: "LastMeetingIMG",
          title: "HHCN",
          template: function (dataItem) {
            return "<span style='background-color: " + dataItem.LastMeetingIMGBgColor + "'>" + dataItem.LastMeetingIMGValue + "</span>";
          },
          headerTemplate: "<span class='app-grid-header'>Hình Họp <br/> Cuối Ngày</span>",
          attributes: {
            class: "app-responsive-font app-cell-highlight",
            style: "text-align: center; background-color: #:LastMeetingIMGBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "90px"
        },
        {
          field: "NumOfOrder",
          title: "DH",
          template: "<img style='background-color: #:NumOfOrderBgColor#' class='app-responsive-icon' src='/images/#:NumOfOrderFlagImg#' /> &nbsp; <span>#:NumOfOrderValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Đơn <br/> Hàng</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:NumOfOrderBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "100px"
        },
        {
          field: "Revenue",
          title: "DS",
          template: "<img style='background-color: #:RevenueBgColor#' class='app-responsive-icon' src='/images/#:RevenueFlagImg#' /> &nbsp; <span>#:RevenueValue#</span>",
          headerTemplate: "<span class='app-grid-header'>Doanh <br/> Số</span>",
          attributes: {
            class: "app-responsive-font",
            style: "text-align: center; background-color: #:RevenueBgColor#",
          },
          headerAttributes: {
            style: "text-align: center;",
            class: "app-responsive-font",
          },
          width: "150px"
        }
      ]
    }

    $("<div/>").attr("class", "app-grid app-grid-detail").appendTo(e.detailCell).kendoGrid({
      dataSource: itemDetailDs,
      scrollable: true,
      sortable: false,
      pageable: false,
      columns: detailGridColumns
    });
  }
})(window.jQuery, window.kendo);
