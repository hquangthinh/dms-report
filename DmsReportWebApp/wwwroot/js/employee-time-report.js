﻿function setDefaultValue(val) {
    if(val && val !== "")
        return val;
    return "4";
}

function formatAsCurrency(val) {
    if(!val)
        return "";
    var num = kendo.parseFloat(val);
    return kendo.format("{0:c}", num);
}

function formatAsNumber(val) {
    if(!val)
        return "";
    var num = kendo.parseFloat(val);
    return kendo.format("{0:n0}", num);
}

function getReportApiUrl() {
  var url = $("#hidReportApiUrl").val();
  if(!url)
  {
    alert("Report url is not valid");
    return "";
  }
  return url;
}

var ssMoneyLevel = "0";
var smMoneyLevel = "0";
var headerWorkingDay = "0";
var avgWorkingRate = "0";
var sumWorkingMoney = "0";
var pageSize = 20;
var dtNow = new Date();
var dtEnd = new Date();
dtEnd.setDate(dtEnd.getDate() + 7);
var startDate = dtNow.getFullYear() + "-" + (dtNow.getMonth() + 1) + "-" + dtNow.getDate();
var endDate = dtEnd.getFullYear() + "-" + (dtEnd.getMonth() + 1) + "-" + dtEnd.getDate();

function createDataSource(reportApiUrl, startDate, endDate) {
    return new kendo.data.DataSource({
        transport: {
            read: {
                url: reportApiUrl,
                type: "POST",
                headers: {
                    "Authorization": "Bearer " + $("#hidAuthToken").val()
                },
                data: function() {
                    return {
                        StartDate: startDate,
                        EndDate: endDate,
                        CustomerID: $("#hidCustomerId").val(),
                        StaffID: $("#hidStaffId").val(),
                        Page: $("#hidPage").val(),
                        PageSize: $("#hidPageSize").val()
                    };
                },
                cache: false,
            },
        },
        requestStart: function(e) {
          $("#loader").show();
        },
        requestEnd: function (e) {
            var response = e.response;
            if(!response)
                return [];
            var avgRate = 0;
            var sumWorkMoney = 0;
            var formatedData = response.map(function(item) {
                item.WorkingMoneyNum = kendo.parseFloat(item.WorkingMoney);
                sumWorkMoney += item.WorkingMoneyNum;
                avgRate += kendo.parseFloat(item.WorkingRate);
                item.WorkingMoney = formatAsNumber(item.WorkingMoney);
                //item.StaffName = item.StaffName + " (GSBH GSBH GSBH)"; // for testing
                if(item.TimeKeepingDetails && Array.isArray(item.TimeKeepingDetails) && item.TimeKeepingDetails.length > 0) {
                    item.TimeKeepingDetails = item.TimeKeepingDetails.map(function(detailItem) {
                        detailItem.NumVisit = setDefaultValue(detailItem.NumVisit);
                        detailItem.DayOFF = setDefaultValue(detailItem.DayOFF);
                        detailItem.OnTime = setDefaultValue(detailItem.OnTime);
                        detailItem.NumStoreIMG = setDefaultValue(detailItem.NumStoreIMG);
                        detailItem.NumOrder = setDefaultValue(detailItem.NumOrder);
                        detailItem.FirstMeetIMG = setDefaultValue(detailItem.FirstMeetIMG);
                        detailItem.LastMeetIMG = setDefaultValue(detailItem.LastMeetIMG);
                        return detailItem;
                    });
                }
                return item;
            });

            if(formatedData.length > 0) {
                ssMoneyLevel = formatAsNumber(formatedData[0].SSMoneyLevel);
                smMoneyLevel = formatAsNumber(formatedData[0].SMMoneyLevel);
                headerWorkingDay = formatedData[0].WorkingDay;
                sumWorkingMoney = formatAsNumber(sumWorkMoney);
                avgWorkingRate = formatAsNumber(avgRate/formatedData.length);
            }

            $("#loader").hide();

            return formatedData;
        },
        pageSize: $("#hidPageSize").val() || pageSize
    });
}

function showDialog(content) {
  if(!content || content === "")
    return;
  $("<div/>").appendTo($("body")).kendoDialog({
    width: "200px",
    title: false,
    closable: false,
    modal: true,
    content: content,
    actions: [
      { text: 'Đóng' },
    ]
  }).data("kendoDialog").open();
}

$(document).ready(function () {
    pageSize = $("#hidPageSize").val();
    startDate = $("#hidDateFrom").val();
    endDate = $("#hidDateTo").val();
    var apiDataSource = createDataSource(getReportApiUrl(), startDate, endDate);

    $("#grid").kendoGrid({
        dataSource: apiDataSource,
        height: window.screen.height - 200,
        mobile: true,
        filterable: true,
        groupable: false,
        sortable: true,
        scrollable: {
            endless: true
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        detailInit: detailInit,
        dataBound: function () {
            this.expandRow(this.tbody.find("tr.k-master-row").first());
            $("#ssMoneyLevel").text(ssMoneyLevel);
            $("#smMoneyLevel").text(smMoneyLevel);
            $("#headerWorkingDay").text(headerWorkingDay);
        },
        columns: [{
            field: "RankNo",
            title: "STT",
            filterable: false,
            headerTemplate: "<span class='app-grid-header'>STT</span>",
            headerAttributes: {
              //class: "app-col-size-30"
              style: "text-align: left;"
            },
            attributes: {
              // class: "app-col-size-30",
              style: "text-align: left;"
            },
            media: "md" // show on tablet or desktop
        },
        {
            field: "StaffName",
            title: "Nhân Viên",
            filterable: { multi: true, search: true},
            template: "<span class='text-primary'><strong>#: StaffName # </strong></span>",
            headerTemplate: "<span class='app-grid-header'>NV</span>",
            footerTemplate: "<span class='text-primary'><strong>Tổng Cộng</strong></span>",
            headerAttributes: {
              // class: "app-col-size-100"
              style: "text-align: left;"
            },
            attributes: {
              // class: "app-col-size-150",
              style: "text-align: left;"
            },
        },
        {
            field: "RealDay",
            title: "NC",
            filterable: false,
            template: "<strong>#: RealDay # </strong>",
            headerTemplate: "<span class='app-grid-header'>NC</span>",
            headerAttributes: {
              // class: "app-col-size-100"
              style: "text-align: right;"
            },
            attributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            }
        },
        {
            field: "CorrectDay",
            title: "NTT",
            filterable: false,
            template: "<strong>#: CorrectDay # </strong>",
            headerTemplate: "<span class='app-grid-header'>NTT</span>",
            headerAttributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            },
            attributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            }
        },
        {
            field: "WorkingRate",
            title: "TT %",
            filterable: false,
            template: "<strong>#: WorkingRate # </strong>",
            headerTemplate: "<span class='app-grid-header'>TT %</span>",
            footerTemplate: "<span class='text-primary'><strong>#:avgWorkingRate#</strong></span>",
            headerAttributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            },
            attributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            },
            footerAttributes: {
              style: "text-align: right;"
            }
        },
        {
            field: "WorkingMoney",
            title: "Tiền CL",
            filterable: false,
            template: "<strong>#: WorkingMoney # </strong>",
            headerTemplate: "<span class='app-grid-header'>Tiền CL</span>",
            format: "{0:c}",
            footerTemplate: "<span class='text-primary'><strong>#:sumWorkingMoney#</strong></span>",
            headerAttributes: {
              // class: "app-col-size-100",
              style: "text-align: right;"
            },
            attributes: {
              // class: "app-col-size-150",
              style: "text-align: right;"
            },
            footerAttributes: {
              style: "text-align: right;"
            }
        },
        {
            field: "WorkingMoney",
            title: "&nbsp;",
            template: "&nbsp;",
            filterable: false,
        }]
    });

    $("#dpFrom").kendoDatePicker({ format: "yyyy-MM-dd" });
    $("#dpTo").kendoDatePicker({ format: "yyyy-MM-dd" });
    $("#btnViewReport").click(function () {
        startDate = $("#dpFrom").val();
        endDate = $("#dpTo").val();
        var grid = $("#grid").data("kendoGrid");
        var ds = createDataSource(getReportApiUrl(), startDate, endDate);
        grid.setDataSource(ds);
        grid.refresh();
    });
});

function detailInit(e) {
    if(!e.data || !e.data.TimeKeepingDetails || e.data.TimeKeepingDetails.length === 0) {
      return;
    }

    var itemDetailDs = {
        data: e.data.TimeKeepingDetails
    };
    $("<div/>").attr("class", "app-grid app-grid-detail").appendTo(e.detailCell).kendoGrid({
        dataSource: itemDetailDs,
        scrollable: true,
        sortable: false,
        pageable: false,
        columns: [
            {
                field: "TimeKeepingDay",
                title: "Ngày",
                template: "<span onclick='showDialog(\"#:Reason#\")'><strong>#:TimeKeepingDay#</strong></span>",
                headerTemplate: "<span class='app-grid-header'>Ngày</span>",
                attributes: {
                    class: "app-responsive-font",
                    style: "color: blue; text-decoration: underline; text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "NumVisit",
                title: "Tuyến",
                template: "<img class='app-responsive-icon' src='images/#:NumVisit#.png' />",
                headerTemplate: "<span class='app-grid-header'>Tuyến</span>",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "OnTime",
                title: "Giờ",
                template: "<img class='app-responsive-icon' src='images/#:OnTime#.png' />",
                headerTemplate: "<span class='app-grid-header'>Giờ</span>",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "NumOrder",
                title: "ĐH",
                template: "<span>#:NumOrder#</span>",
                headerTemplate: "<span class='app-grid-header'>ĐH</span>",
                attributes: {
                    style: "text-align: center; color: #:NumOrderHex#; font-weight: bold;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "NumStoreIMG",
                title: "HĐB",
                template: "<img class='app-responsive-icon' src='images/#:NumStoreIMG#.png' />",
                headerTemplate: "<span class='app-grid-header'>HĐB</span>",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "FirstMeetIMG",
                title: "HH",
                template: "<img class='app-responsive-icon' src='images/#:FirstMeetIMG#.png' />",
                headerTemplate: "<span class='app-grid-header'>HH</span>",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            },
            {
                field: "LastMeetIMG",
                title: "HHC",
                template: "<img class='app-responsive-icon' src='images/#:LastMeetIMG#.png' />",
                headerTemplate: "<span class='app-grid-header'>HHC</span>",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                }
            }
        ]
    });
}
