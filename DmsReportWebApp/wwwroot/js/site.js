﻿function setDefaultValue(val) {
  if(val && val !== "")
    return val;
  return "4";
}

function formatAsCurrency(val) {
  if(!val)
    return "";
  var num = kendo.parseFloat(val);
  return kendo.format("{0:c}", num);
}

function formatAsNumber(val) {
  if(!val)
    return "";
  var num = kendo.parseFloat(val);
  return kendo.format("{0:n0}", num);
}

function formatAsMoneyNumber(val) {
  if(!val)
    return "";
  var num = kendo.parseFloat(val);
  return kendo.format("{0:n2}", num);
}

function getReportApiUrl() {
  var url = $("#hidReportApiUrl").val();
  if(!url)
  {
    alert("Report url is not valid");
    return "";
  }
  return url;
}

// "#ffffff||K.Đ" => arr[2]
// "#ffffff||K.Đ|5" => arr[3]
function parseMultiValueField(sValue) {
  if(!sValue)
    return ['#ffffff', '', ''];
  var arr = sValue.split('|');
  var filtered = arr.filter(function (el) {
    return el != null && el !== '';
  });
  return filtered;
}
