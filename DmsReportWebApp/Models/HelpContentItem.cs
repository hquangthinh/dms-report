﻿namespace DmsReportWebApp.Models
{
    public class HelpContentItem
    {
        public string ImageUrl { get; set; }
        public string ImageCaption { get; set; }
    }
}